from math import log
from numpy import zeros
import gc

@profile
def main_F():
    ps_count = 1e-30
    num_states = 8
    # init_prob = zeros((8))
    # init_prob.fill(ps_count)
    init_prob = []

    transition_prob = zeros((8,8))
    # transition_prob.fill(ps_count)

    emission = zeros((4,8))
    # emission.fill(ps_count)

    # print(emission)

    def convert(c):
        if c == 'A':
            return 0
        elif c == 'C':
            return 1
        elif c == 'G':
            return 2
        elif c == 'T':
            return 3
        return 0

    file = open('input.txt','r')
    strings = file.read().splitlines()

    sequence = strings[-1]
    leng = len(sequence)

    for item in strings[:num_states]:
        init_prob.append(float(item.split()[-1]))

    idy = 0
    for i in range(8):
        idx = 0
        for item in strings[num_states + (i*8) : num_states + ((i+1)*8)]:
            transition_prob[idy][idx] = float(item.split()[-1])
            idx += 1
        idy += 1

    del(file)
    del(strings)
    gc.collect()

    # Emission
    for i in range(len(emission)):
        for j in range(len(emission[0])):
            emission[i][j] = log((1.0 if i == j % 4 else ps_count))

    # Initialize matrix and traceback
    # len = len(sequence)

    DP_MAT = zeros((num_states, leng))
    # DP_MAT.fill(ps_count)
    traceback = zeros((num_states, leng))

    for i in range(num_states):
        e = emission[convert(sequence[0])][i]
        DP_MAT[i][0] = e + init_prob[i]

    # Filling the Matrix and traceback
    for i in range(1, leng):
        for j in range(num_states):
            res = -1000000.0
            res_id = 0
            for k in range(num_states):
                _val = DP_MAT[k][i - 1] + transition_prob[k][j]
                if (_val > res):
                    res = _val
                    res_id = k
                    
            DP_MAT[j][i] = res
            traceback[j][i] = res_id
            DP_MAT[j][i] += emission[convert(sequence[i])][j]    
                
    del(emission)
    gc.collect()
    
    # res
    res = zeros((leng), dtype=int)
    _res = -1000000
    _var = -10000000.0

    for k in range(num_states):
        if DP_MAT[k][leng - 1] > _var:
            _res = k
            _var = DP_MAT[k][leng - 1]
            
    res[leng - 1] = _res   

    # Filling res vector
    for i in range(1, 26)[::-1]:
        res[i - 1] = traceback[res[i]][i]

    # Finding coordinates
    start = 0
    end = 0
    for i in range(leng):
        if ((i == 0 and res[i] >= 4) or (res[i] >= 4 and res[i - 1] < 4)):
            start = i
        elif  ((i + 1 == leng and res[i] >= 4) or (res[i] >= 4 and res[i + 1] < 4)):
            end = i
            print(start + 1, end + 1)

if __name__ == '__main__':
    main_F()
        
