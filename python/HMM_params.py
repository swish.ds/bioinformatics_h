import math
import numpy as np

file = open('input1.txt','r')
strings = file.read().splitlines()

cpg_range = []

for i in strings[:-1]:
    cpg_range.append(tuple(map(int, i.split())))

sequence = strings[-1]

states = np.zeros((4))

def inside_cpg(pos):
    for p in cpg_range:
        if pos >= p[0] and pos <= p[-1]:
            return True
        
    return False

def convert(c):
    if c == 'A':
        return 0
    elif c == 'C':
        return 1
    elif c == 'G':
        return 2
    elif c == 'T':
        return 3
    
    return 0

def row_sum(i):
    res = 0
    for j in range(len(mat[0])):
        res += mat[i][j]
    
    return res

negative = 0
positive = 0
i = 0
for c in sequence:
    b = inside_cpg(i+1)
    if b:
        positive += 1
    else:
        negative += 1
        
    if not b:
        states[convert(c)] += 1
        
    i += 1        

# начальные вероятности для каждого state
for v in states:
    s = str(round(math.log((v + 1e-30) / (1 + negative)) * 100000.0) / 100000.0)
    print(s)

# переходные вероятности для каждого state1 - state2
mat = np.zeros((8, 8))

for i in range(1, len(sequence)):
    c1 = sequence[i-1]
    c2 = sequence[i]
    
    b1 = inside_cpg(i)
    b2 = inside_cpg(i+1)
    
    if b1:
        shift1 = 4
    else:
        shift1 = 0
    
    if b2:
        shift2 = 4
    else:
        shift2 = 0
        
    mat[convert(c1) + shift1][convert(c2) + shift2] += 1
    
# Расчитываем переходные вероятности для каждого state1 - state2
for i in range(len(mat)):
    for j in range(len(mat[0])):
        sum_i = row_sum(i)
        s = str(round(math.log((mat[i][j] + 1e-30) / (1.0 + sum_i)) * 100000.0) / 100000.0)
        print(s)