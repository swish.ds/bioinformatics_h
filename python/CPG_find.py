import math
import numpy as np

ZERO = 1e-30
STATE_SZ = 8
# init_prob = np.zeros((8))
# init_prob.fill(ZERO)
init_prob = []

transition_prob = np.zeros((8,8))
transition_prob.fill(ZERO)

emission = np.zeros((4,8))
emission.fill(ZERO)

# print(emission)

def convert(c):
    if c == 'A':
        return 0
    elif c == 'C':
        return 1
    elif c == 'G':
        return 2
    elif c == 'T':
        return 3
    return 0

file = open('input.txt','r')
strings = file.read().splitlines()

sequence = strings[-1]

for item in strings[:STATE_SZ]:
    init_prob.append(float(item.split()[-1]))

idy = 0
for i in range(8):
    idx = 0
    for item in strings[STATE_SZ + (i*8) : STATE_SZ + ((i+1)*8)]:
        transition_prob[idy][idx] = float(item.split()[-1])
        idx += 1
    idy += 1

del(file)
del(strings)

# Emission
for i in range(len(emission)):
    for j in range(len(emission[0])):
        emission[i][j] = math.log((1.0 if i == j % 4 else ZERO))

# Initialize matrix and trace
len = len(sequence)

M = np.zeros((STATE_SZ, len))
M.fill(ZERO)
trace = np.zeros((STATE_SZ, len))

for i in range(STATE_SZ):
    e = emission[convert(sequence[0])][i]
    M[i][0] = e + init_prob[i]

# Filling the Matrix and trace
for i in range(1, len):
    for j in range(STATE_SZ):
        res = -1000000.0
        resId = 0
        for k in range(STATE_SZ):
            _v = M[k][i - 1] + transition_prob[k][j]
            if (_v > res):
                res = _v
                resId = k
                
        M[j][i] = res
        trace[j][i] = resId
        M[j][i] += emission[convert(sequence[i])][j]    
            
# res
res = np.zeros((len), dtype=int)
_res = -1000000
_var = -10000000.0

for k in range(STATE_SZ):
    if M[k][len - 1] > _var:
        _res = k
        _var = M[k][len - 1]
        
res[len - 1] = _res   

# Filling res vector
for i in range(1, 26)[::-1]:
    res[i - 1] = trace[res[i]][i]

# Finding coordinates
start = 0
end = 0

for i in range(len):
    
    if ((i == 0 and res[i] >= 4) or (res[i] >= 4 and res[i - 1] < 4)):
       start = i
    elif  ((i + 1 == len and res[i] >= 4) or (res[i] >= 4 and res[i + 1] < 4)):
        end = i
        print(start + 1, end + 1)
