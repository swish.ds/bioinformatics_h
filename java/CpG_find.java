import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CpG_find {

    public static int converti(char c) {
        if (c == 'A'){
            return 0;
        }
        if (c == 'C'){
            return 1;
        }
        if (c == 'G'){
            return 2;
        }
        if (c == 'T'){
            return 3;
        }

        return 0;
    }
    
    public static void main(String[] args) throws IOException {
        
        int num_states = 8;
        float ps_count = (float)1e-30;

        Scanner inFile1 = new Scanner(new File("input.txt"));
        inFile1.useDelimiter(",\\s*");

        List<String> strings = new ArrayList<String>();

        while (inFile1.hasNext()) {
            strings.add(inFile1.nextLine());
        }

        String sequence = strings.get(strings.size() - 1);

        List<Float> init_probs = new ArrayList<Float>();

        for(String s : strings.subList(0, num_states)) {
            init_probs.add(Float.parseFloat(s.split(" ")[s.split(" ").length - 1]));
        }

        int idy = 0;
        List<List<Float>> transition_prob = new ArrayList<List<Float>>();

        for(int i = 0; i < 8; i++) {
            transition_prob.add(new ArrayList<Float>());
            for(String s : strings.subList(num_states + (i*8), num_states + ((i+1)*8))) {
                transition_prob.get(idy).add(Float.parseFloat(s.split(" ")[s.split(" ").length - 1]));
            }
            idy ++;
        }


        List<List<Float>> emission = new ArrayList<List<Float>>();

        for (int i = 0; i < 4; i++) {
            emission.add(new ArrayList<Float>());
            for (int j = 0; j < 8; j++) {
                emission.get(i).add((float)Math.log((i == j % 4) ? 1.0 : ps_count));
            }
            
        }

        int leng = sequence.length(); //26
        double[][] DP_MAT = new double[num_states][leng];
        int[][] traceback = new int[num_states][leng];

        for (int i = 0; i < num_states; i++) {
            double e = emission.get(converti(sequence.charAt(0))).get(i);
            DP_MAT[i][0] = e + init_probs.get(i);
        }

        double res;
        int res_id;

        for (int i = 1; i < leng; i++) {
            for (int j = 0; j < num_states; j++) {
                res = -1000000.0;
                res_id = 0;
                for (int k = 0; k < num_states; k++) {
                    double _v = DP_MAT[k][i - 1] + transition_prob.get(k).get(j);
                    if (_v > res) {
                        res = _v;
                        res_id = k;
                    }
                }
                DP_MAT[j][i] = res;
                traceback[j][i] = res_id;
                DP_MAT[j][i] += emission.get(converti(sequence.charAt(i))).get(j);;
            }
        }

        int[] ress = new int[leng];

        int _res = -1000000;
        double _var = -10000000.0;
        for (int k = 0; k < num_states; k++) {
            if (DP_MAT[k][leng-1] > _var) {
                _res = k;
                _var = DP_MAT[k][leng - 1];
            }
        }
        ress[leng - 1] = _res;

        

        for (int i = leng-1; i>0; i--) {
            ress[i - 1] = traceback[ress[i]][i];
        }
        
        int start = 0;
        int end = 0;
        for (int i = 0; i < leng; i++) {
            if ((i == 0 && ress[i] >= 4) || (ress[i] >= 4 && ress[i-1] < 4)) {
                start = i;
            } else if ((i+1 == leng && ress[i] >= 4) || (ress[i] >=4 && ress[i+1] < 4)){
                end = i;
                System.out.println((start + 1) + " " + (end + 1));
            }
        }
    }    
}